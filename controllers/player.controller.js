const Player = require("../models/player.model");

// Create and Save a new Upgrade
exports.connect = (req, res) => {
    Player.connect(req.body.login, req.body.password, (err, data) => {
        if (err) {
            res.status(500).send({
                message:
                    err.message || "Nous n'avons pas réussi à vous connecter"
            });
        } else {
            res.json(data);
        }
    });
};

// Verify Token
exports.verifyToken = (req, res) => {
    Player.verifyToken(req.body.login, req.body.token, (err, data) => {
        if (err) {
            res.status(500).send({
                message:
                    err.message || "Nous ne parvenons pas à vérifier votre token"
            });
        } else {
            res.json(data);
        }
        console.log(data);
    });
};

// Update Token
exports.updateToken = (req, res) => {
    Player.updateToken(req.body.login, req.body.token, (err, data) => {
        if (err) {
            res.status(500).send({
                message:
                    err.message || "Nous ne parvenons pas à vérifier votre token"
            });
        } else {
            res.json(data);
        }
        console.log(data);
    });
};

exports.save = (req, res) => {
    Player.save(req.body.id_player, req.body.money, req.body.experience, (err, data) => {
        if (err) {
            res.status(500).send({
                message:
                    err.message || "Nous n'avons pas réussi à vous connecter"
            });
        } else {
            res.json(data);
        }
    });
};
