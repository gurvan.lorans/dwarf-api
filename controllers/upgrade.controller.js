const Upgrade = require("../models/upgrade.model");

// Find all upgrade links with a player
exports.findByPlayer = (req, res) => {
    Upgrade.findByPlayer(req.params.player_id, (err, data) => {
        if (err) {
            res.status(500).send({
                message: err.message || "Nous n'avons pas réussi à retrouver les amélioriations en fonction du joueur"
            });
        } else {
            res.json(data)
        }
    });
}

exports.buyUpgrade = (req, res) => {
    Upgrade.buyUpgrade(req.body.id_upgrade, req.body.id_player, req.body.nb_to_buy, (err, data) => {
        if (err) {
            res.status(500).send({
                message: err.message || "Nous n'avons pas réussi à retrouver les amélioriations en fonction du joueur"
            });
        } else {
            res.json(data)
        }
    });
}
