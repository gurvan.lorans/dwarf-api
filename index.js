const express = require('express');
const cors = require('cors');
const app = express();
const conf = require('./conf');

app.use(cors());
app.use(express.json());
app.use(require('body-parser').urlencoded({ extended: false }));


require("./routes/upgrade.routes")(app);
require("./routes/player.routes")(app);

app.listen(conf.PORT, conf.HOST, () => {
    console.log("Serveur à l'écoute");
})
