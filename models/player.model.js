const sql = require("../db.js");

class Player {
    constructor(player) {
        this.id_player = player.id_player;
        this.login = player.login;
        this.password = player.password;
        this.money = player.money;
        this.experience = player.experience;
    }

    static connect (login, password, result) {
        sql.query("SELECT id_player, login, money, experience  FROM dwarf_clicker.player WHERE login LIKE ? AND password LIKE ?", [login, password],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }

            if(res.length > 0){
                result(null, {status: "connected", player: res})
            } else {
                result(null, {status: "cannot connect"});
            }
            console.log(res)
        });
    };

    static verifyToken (login, token, result) {
        sql.query("SELECT id_player, login, money, experience  FROM dwarf_clicker.player WHERE login LIKE ? AND token LIKE ?", [login, token],
        (err, res) => {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }

            if(res.length > 0){
                result(null, {status: "connected", player: res})
            } else {
                result(null, {status: "cannot connect"});
            }
        });
    }

    static updateToken (login, token, result) {
        sql.query("UPDATE dwarf_clicker.player SET token = ? WHERE login LIKE ?;", [token, login],
            (err, res) => {
                if (err) {
                    console.log("error: ", err);
                    result(err, null);
                    return;
                }

                result(null, res)
            }
        );
    }

    static save (id_player, money, experience, result) {
        sql.query("UPDATE dwarf_clicker.player SET money = ?, experience = ? WHERE id_player = ?", [money, experience, id_player],
        (err, res) => {
            if(err){
                console.log("error: ", err);
                result(null, err);
                return;
            }

            console.log('[dwarf_api.player.save]: ', res);
            result(null, res);
        });
    };
}

module.exports = Player;
