const sql = require("../db.js");

class Upgrade {

    constructor(upgrade) {
        this.id_upgrade = upgrade.id_upgrade;
        this.bonus_type = upgrade.bonus_type;
        this.bonus_amount = upgrade.bonus_amount;
        this.name = upgrade.name;
        this.description = upgrade.description;
        this.image_link = upgrade.image_link;
    }

    static findByPlayer (player_id, result) {
        sql.query("SELECT upgrade.id_upgrade, bonus_type, bonus_amount, name, description, base_price, image_link, pu.nb_bought FROM dwarf_clicker.upgrade LEFT JOIN dwarf_clicker.player_upgrades pu ON upgrade.id_upgrade = pu.id_upgrade AND pu.id_player = "+player_id, (err, res) => {

            if(err){
                console.log("error: ", err);
                result(null, err);
                return;
            }

            console.log('[dwarf_api.upgrade.findByPlayer]: ', res);
            result(null, res);
        });
    }

    static buyUpgrade (player_id, upgrade_id, nb_to_buy, result) {
        sql.query("INSERT INTO dwarf_clicker.player_upgrades(id_player, id_upgrade, nb_bought) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE nb_bought = nb_bought + ?;", [upgrade_id, player_id, nb_to_buy, nb_to_buy], (err, res) => {

            if(err){
                console.log("error: ", err);
                result(null, err);
                return;
            }

            console.log('[dwarf_api.upgrade.buy]: ', res);
            result(null, res);
        });
    }

}

module.exports = Upgrade;
