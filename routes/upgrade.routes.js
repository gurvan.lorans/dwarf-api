module.exports = app => {
    const upgrade = require("../controllers/upgrade.controller.js");

    // Search upgrade links to player
    app.get("/upgrade/:player_id", upgrade.findByPlayer);
    app.put("/upgrade/buy", upgrade.buyUpgrade);


};
