module.exports = app => {
    const player = require("../controllers/player.controller.js");

    // connect a player
    app.post("/player/connect", player.connect);
    app.post("/player/token/verify", player.verifyToken);
    app.post("/player/token/update", player.updateToken);
    app.post("/player/save", player.save);

};
